//
//  DetailViewController.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var label_name: UILabel!
    @IBOutlet weak var textView_description: UITextView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var label_avatar_name: UILabel!
    @IBOutlet weak var label_camera: UILabel!
    @IBOutlet weak var constraint_height_description: NSLayoutConstraint!
    @IBOutlet weak var map: MKMapView!
    
    var image500 : Image500!
    
    init(img500 : Image500)
    {
        super.init(nibName: nil, bundle: nil)
        image500 = img500
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        label_name.text = image500.name
        if(image500.desc != ""){
            //Perform workaround
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), {
                do {
                    let str = try NSAttributedString(data: self.image500.desc.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                    dispatch_async(dispatch_get_main_queue(), {
                        self.textView_description.attributedText = str
                    })
                    self.textView_description.delegate = self
                } catch {
                    self.textView_description.text = ""
                    dispatch_async(dispatch_get_main_queue(), {
                        self.constraint_height_description.constant = 0
                    })
                    print(error)
                }
            })
        }
        else{
            self.constraint_height_description.constant = 0
        }
        label_camera.text = image500.camera
        label_avatar_name.text = image500.username
        imageAvatar.load(NSURL(string: image500.avatar)!)
        if(image500.latitude.floatValue > 0 && image500.longitude.floatValue > 0){
            configMap()
        }
        else{
            map.hidden = true
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_back(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    //Pragma mark - Others
    
    func configMap(){
        let initialLocation = CLLocation(latitude: CLLocationDegrees(image500.latitude), longitude: CLLocationDegrees(image500.longitude))
        func centerMapOnLocation(location: CLLocation) {
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                10000, 10000)
            map.setRegion(coordinateRegion, animated: true)
        }
        centerMapOnLocation(initialLocation)
        //Annotation
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(image500.latitude), longitude: CLLocationDegrees(image500.longitude))
        map.addAnnotation(annotation)
    }
    
    //Pragma mark - textView
    
    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        UIApplication.sharedApplication().openURL(URL)
        return true
    }
    
}
