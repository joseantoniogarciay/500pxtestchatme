//
//  DataProvider.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import Foundation

var URL_PHOTOS : String = "https://api.500px.com/v1/photos?feature=popular";
var CONSUMER_KEY : String = "25iV3G1tir2osMfZkO6RHiojqkKqx404CE8Po6pg";
var KEY_CONSUMERKEY : String = "consumer_key";

func GetJSONWithUrl( var urlString : String, params : NSDictionary?, completionClosure: (success : Bool, responseObject : AnyObject?, error : NSError?) ->()){
    
    if(params != nil ){
        for (key, object) in params! {
            urlString = urlString + "&" + (key as! String) + "=" + (object as! String)
        }
    }
    
    let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
    let session = NSURLSession.sharedSession()
    request.HTTPMethod = "GET"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTaskWithRequest(request) { data, response, error in
        guard data != nil else {
            print("no data found: \(error)")
            dispatch_async(dispatch_get_main_queue()) {
                completionClosure(success: false, responseObject: nil, error: error)
            }
            return
        }
        do {
            if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                print("Success: \(json) \n")
                dispatch_async(dispatch_get_main_queue()) {
                    completionClosure(success: true, responseObject: json, error: nil)
                }
            }
            else {
                let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("Error could not parse JSON: \(jsonStr) \n")
                dispatch_async(dispatch_get_main_queue()) {
                    completionClosure(success: false, responseObject: nil, error: error)
                }
            }
        }
        catch let parseError {
            print(parseError)
            let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Error could not parse JSON: '\(jsonStr)' \n")
            dispatch_async(dispatch_get_main_queue()) {
                completionClosure(success: false, responseObject: nil, error: error)
            }
        }
    }
    task.resume()
}
