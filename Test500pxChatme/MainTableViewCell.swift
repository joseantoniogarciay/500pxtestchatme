//
//  MainTableViewCell.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import UIKit
import ImageLoader

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var centeredView: UIView!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        centeredView.layer.cornerRadius = 10.0
        centeredView.layer.borderColor = UIColor.grayColor().CGColor
        centeredView.layer.borderWidth = 0.5
        centeredView.clipsToBounds = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configCell(image500 : Image500){
        rating.text = "\(image500.rating)"
        name.text = image500.name
        imageCell.load(NSURL(string: image500.url)!)
    }
    
}
