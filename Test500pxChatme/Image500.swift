//
//  Image500.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import UIKit
import CoreData

class Image500: NSManagedObject {
    
    @NSManaged var name: String
    @NSManaged var url: String
    @NSManaged var rating: NSNumber
    @NSManaged var desc: String
    @NSManaged var camera: String
    @NSManaged var avatar: String
    @NSManaged var username: String
    @NSManaged var latitude: NSNumber
    @NSManaged var longitude: NSNumber
    
    convenience init(info: NSDictionary, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        let entity = NSEntityDescription.entityForName("Image500", inManagedObjectContext: context)!
        self.init(entity: entity, insertIntoManagedObjectContext: context)
        
        name = info["name"] as? String ?? ""
        url = info["image_url"] as? String ?? ""
        rating = info["rating"] as? NSNumber ?? 0
        desc = info["description"] as? String ?? ""
        camera = info["camera"] as? String ?? ""
        avatar = info["user"]!["avatars"]!!["default"]!!["https"] as? String ?? ""
        username = info["user"]!["username"] as? String ?? ""
        latitude = info["latitude"] as? NSNumber ?? 0
        longitude = info["longitude"] as? NSNumber ?? 0
        
        do {
            try context.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
    
}
