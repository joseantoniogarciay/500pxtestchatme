//
//  MainViewController.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import UIKit
import CoreData


class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    var refreshControl:UIRefreshControl!
    var dataArray : NSMutableArray!
    var blurEffectView : UIVisualEffectView!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.alpha = 0
        dataArray = NSMutableArray()
        self.automaticallyAdjustsScrollViewInsets = false;
        
        //Tableview
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "MainTableViewCell", bundle:nil), forCellReuseIdentifier: "MainCell")
        tableView.contentInset = UIEdgeInsetsMake(topView.frame.size.height, 0, 0, 0)
        tableView.scrollIndicatorInsets = UIEdgeInsetsMake(topView.frame.size.height, 0, 0, 0)
        
        //RefreshControl
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.whiteColor()
        let text : NSMutableAttributedString =  NSMutableAttributedString(string: "Pull to refresh")
        text.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: NSMakeRange(0, text.length))
        refreshControl.attributedTitle = text
        refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        
        //getInfo
        self.getInfo()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if(blurEffectView == nil){
            //TopView Blur Effect
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = topView.bounds
            topView.addSubview(blurEffectView)
            topView.backgroundColor = UIColor(rgb: 0xEFEFEF, alpha: 0.8)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Pragma mark - Tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : MainTableViewCell = tableView.dequeueReusableCellWithIdentifier("MainCell", forIndexPath: indexPath) as! MainTableViewCell
        cell.configCell(dataArray[indexPath.row] as! Image500)
        cell.imageCell.userInteractionEnabled = true
        cell.imageCell.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "imageTapped:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        cell.imageCell.addGestureRecognizer(tapGestureRecognizer)
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 230
    }
    
    //Pragma mark - Others
    
    func imageTapped(sender: UITapGestureRecognizer) {
        let detailVC = DetailViewController(img500: dataArray[(sender.view?.tag)!] as! Image500)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func refresh(sender:AnyObject){
        self.getInfo()
    }
    
    func getInfo(){
        let params = NSDictionary(objects: [CONSUMER_KEY], forKeys: [KEY_CONSUMERKEY])
        GetJSONWithUrl(URL_PHOTOS, params: params) { [weak self] (success, responseObject, error) -> ()  in
            if(success){
                if let result = responseObject as? NSDictionary{
                    let arrPhotos = result.objectForKey("photos") as? NSArray
                    if let strongSelf = self {
                        //Clean BBDD
                        let fetchRequest = NSFetchRequest(entityName: "Image500")
                        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                        do {
                            try self!.managedObjectContext.persistentStoreCoordinator!.executeRequest(deleteRequest, withContext: self!.managedObjectContext)
                        } catch let error as NSError {
                            print("BBDD Error \(error.description)")
                        }
                        
                        //Create again
                        for ( _ , element) in arrPhotos!.enumerate() {
                            let newImage500 = Image500(info: element as! NSDictionary, insertIntoManagedObjectContext: self!.managedObjectContext)
                            strongSelf.dataArray.addObject(newImage500)
                        }
                        strongSelf.tableView.reloadData()
                        strongSelf.refreshControl.endRefreshing()
                        strongSelf.tableviewAnimation()
                    }
                }
                else{
                    //Good practices
                    if let strongSelf = self {
                        showAlert(strongSelf, mytitle: "Format Error, loaded local data", mymessage: "")
                        strongSelf.loadLocalBBDD()
                        strongSelf.refreshControl.endRefreshing()
                        strongSelf.tableviewAnimation()
                    }
                }
            }
            else{
                //Good practices
                if let strongSelf = self {
                    showAlert(strongSelf, mytitle: "Connection Error, loaded local data", mymessage: "")
                    strongSelf.loadLocalBBDD()
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.tableviewAnimation()
                }
            }
        }
    }
    
    
    func loadLocalBBDD(){
        let fetchRequest = NSFetchRequest(entityName: "Image500")
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "rating", ascending: false)
        fetchRequest.sortDescriptors = [descriptor]
        do {
            let list = try managedObjectContext.executeFetchRequest(fetchRequest)
            dataArray.addObjectsFromArray(list)
            tableView.reloadData()
            self.tableviewAnimation()
        }
        catch let error as NSError {
            // failure
            print("Fetch failed: \(error.localizedDescription)")
        }
    }
    
    func tableviewAnimation(){
        UIView.animateWithDuration(1.0, delay: 1.2, options: .CurveEaseOut, animations: {
            self.tableView.alpha = 1
            }, completion: { finished in
                print("Animation done!")
        })
    }
}
