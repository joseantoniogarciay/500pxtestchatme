//
//  Utils.swift
//  Test500pxChatme
//
//  Created by Jose Antonio Garcia Yáñez on 12/11/15.
//  Copyright © 2015 JoseAntonio. All rights reserved.
//

import UIKit

func showAlert(controller: UIViewController?, mytitle : String , mymessage : String){
    let alertController = UIAlertController(title: mytitle as String, message: mymessage as String, preferredStyle: .Alert)
    
    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertController.addAction(defaultAction)
    
    controller?.presentViewController(alertController, animated: true, completion: nil)
}
