# README #

This is a test developed in Swift 2.1 language and Xcode 7.1.
Using CocoaPods for handle dependencies like an image loader asynchronously.

This project uses CoreData, animation, GCD, AutoLayout, extensions, UIRefreshControl, UIBlurEffect, a personal Network manager and a lot of basics of UIKit.

@joseantoniogarciay 